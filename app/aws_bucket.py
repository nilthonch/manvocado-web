import logging
import json
import boto3
from app.mongo import Database
from flask import Response
from botocore.exceptions import ClientError
from PIL import Image
import io
import base64


def upload_file(file_name, n_img):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    with open ('config.json') as file:
        data = json.load(file)
        key = data['awsKey']
        secret = data['awsSecret']

    file_name = file_name.replace(' ', '+')
    pil_image = Image.open(io.BytesIO(base64.b64decode(file_name)))

    # Save the image to an in-memory file
    in_mem_file = io.BytesIO()
    pil_image.save(in_mem_file, format=pil_image.format)
    in_mem_file.seek(0)

    # Upload the file
    session = boto3.session.Session()
    s3_client = session.client(
                's3',
                aws_access_key_id=key,
                aws_secret_access_key=secret
                )

    existent_elements = s3_client.list_objects(Bucket='manvocado')

    try:
        print(len(existent_elements['Contents']))
        n = len(existent_elements['Contents']) + n_img
    except:
        n = 0

    try:
        response = s3_client.upload_fileobj(in_mem_file, "manvocado", "image" + str(n) + ".png", ExtraArgs={'ACL':'public-read'})
    except ClientError as e:
        logging.error(e)
        return False


    link = "https://manvocado.s3.eu-west-3.amazonaws.com/image" + str(n) + ".png"
    return link

def remove_file():
    """
    Remove the last image uploaded to the Bucket
    """

    client = Database()

    with open ('config.json') as file:
        data = json.load(file)
        key = data['awsKey']
        secret = data['awsSecret']


    # Upload the file
    s3_client = boto3.client(
                's3',
                aws_access_key_id=key,
                aws_secret_access_key=secret
                )

    existent_elements = s3_client.list_objects(Bucket='manvocado')


    print(len(existent_elements['Contents']))
    n = len(existent_elements['Contents'])


    try:
        s3_client.delete_object(Bucket='manvocado', Key='image' + str(n-1) + '.png')
        client.remove("https://manvocado.s3.eu-west-3.amazonaws.com/image" + str(n-1) + ".png")
    except ClientError as e:
        logging.error(e)
        return False

    response = {'Success': 200}
    
    return Response(json.dumps(response), mimetype='application/json', status=200)